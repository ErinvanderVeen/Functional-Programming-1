Zij gegeven:

(++) :: [a] [a] -> [a]
(++) []     xs = xs                (1)
(++) [y:ys] xs = [y : ys ++ xs]    (2)

map :: (a -> b) [a] -> [b]
map f []       = []                (3)
map f [x:xs]   = [f x : map f xs]  (4)

flatten :: [[a]] -> [a]
flatten []     = []                (5)
flatten [x:xs] = x ++ (flatten xs) (6)

1. 
Te bewijzen: 
	Voor iedere functie f, eindige lijst as en bs:
		
		map f (as ++ bs) = (map f as) ++ (map f bs)

Bewijs:
	Volledige inductie over as:

	Basis:
		Stel as = [], dan:

			map f (as ++ bs) 										// Aanname
							= map f ([] ++ bs)						// (1)
							= map f bs								// (1)
							= [] ++ (map f bs)						// (3)
							= (map f []) ++ (map f bs)				// Aanname
							= (map f as) ++ (map f bs)

	Inductiestap:
		Inductiehypothese: map f (as ++ bs) = (map f as) ++ (map f bs)

			map f ([a : as] ++ bs)									// (2)
							= map f [a : as ++ bs]					// (4)
							= [f a : map f (as ++ bs)]				// IH
							= [f a : (map f as) ++ (map f bs)]		// (2)
							= [f a : map f as] ++ (map f bs)		// (4)
							= (map f [a : as]) ++ (map f bs)

	Hieruit volgt dat voor iedere eindige lijst as, bs en iedere functie f geldt: map f (as ++ bs) = (map f as) ++ (map f bs)

2. 
Te bewijzen:
	Voor iedere functie f, voor iedere eindige lijst xs:
	
		flatten (map (map f) xs) = map f (flatten xs)

Bewijs:
	Volledige inductie over xs:

	Basis:
		Stel xs = [], dan:

			flatten (map (map f) xs)								// Aanname
							= flatten (map (map f) [])				// (3)
							= flatten []							// (5)
							= []									// (3)
							= map f []								// (5)
							= map f (flatten [])					// Aanname
							= map f (flatten xs)

	Inductiestap:
		Inductiehypothese: flatten (map (map f) xs) = map f (flatten xs)

			flatten (map (map f) [x : xs])							// (4)
							= flatten [map f x : map (map f) xs]	// (6)
							= (map f x) ++ (flatten map (map f) xs)	// IH
							= (map f x) ++ (map f (flatten xs))		// Stelling 1
							= map f (x ++ (flatten xs))				// (6)
							= map f (flatten [x : xs])

	Hieruit volgt dat voor iedere eindige lijst xs en iedere functie f geldt: flatten (map (map f) xs) = map f (flatten xs).

	