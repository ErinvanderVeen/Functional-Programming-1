implementation module VectorOverloading

import StdEnv

:: Vector2 a = {x0 :: a, x1 :: a}

instance ==   (Vector2 a) | == a
	where
	(==) v0 v1 = v0.x0 == v1.x0 && v0.x1 == v1.x1

instance zero (Vector2 a) | zero a
	where
	zero = {x0 = zero, x1 = zero}

instance one  (Vector2 a) | one a
	where
	one = {x0 = one, x1 = one}

instance ~    (Vector2 a) | ~ a
	where
	(~) v = {x0 = ~v.x0, x1 = ~v.x1}

instance +    (Vector2 a) | + a
	where
	(+) v0 v1 = {x0 = v0.x0 + v1.x0, x1 = v0.x1 + v1.x1}

instance -    (Vector2 a) | - a
	where
	(-) v0 v1 = {x0 = v0.x0 - v1.x0, x1 = v0.x1 - v1.x1}

// This is not a form of vector multiplication since there exists no form of vector multiplication
// on two vectors in 2D-space that returns another vector
instance *    (Vector2 a) | * a
	where
	(*) v0 v1 = {x0 = v0.x0 * v1.x0, x1 = v0.x1 * v1.x1}

// There exists no formal form of vector division
instance /    (Vector2 a) | / a
	where
	(/) v0 v1 = {x0 = v0.x0 / v1.x0, x1 = v0.x1 / v1.x1}

