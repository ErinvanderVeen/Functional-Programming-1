implementation module StdQ

import StdEnv

:: Q = Frac Int Int

instance == Q 
	where 
	== (Frac n0 d0) (Frac n1 d1) = n0 * d1 == n1 * d0

instance < Q
	where
	< (Frac n0 d0) (Frac n1 d1) = n0 * d1 < n1 * d0

instance + Q
	where
	+ (Frac n0 d0) (Frac n1 d1) = Frac (n0 * d1 + n1 * d0) (d0 * d1)

instance - Q
	where
	- (Frac n0 d0) (Frac n1 d1) = Frac (n0 * d1 - n1 * d0) (d0 * d1)

instance zero Q
	where
	zero = Frac 0 1

instance * Q
	where
	* (Frac n0 d0) (Frac n1 d1) = Frac (n0 * n1) (d0 * d1)

instance / Q
	where
	/ q (Frac n1 d1) = q * (Frac d1 n1)

instance one Q
	where
	one = Frac 1 1

instance abs Q
	where
	abs (Frac n d) = Frac (abs n) (abs d)

instance sign Q
	where
	sign (Frac n d)
	| n * d > 0 = 1
	| n * d < 0 = -1
	| otherwise = 0

instance ~ Q
	where
	~ (Frac n d) = Frac (~n) d
	
isInt :: Q -> Bool
isInt (Frac n d)
| n rem d == 0 = True
| otherwise = False

instance toInt Q
	where
	toInt (Frac n d) = n / d

instance toReal Q
	where
	toReal (Frac n d) = toReal n / toReal d

instance toQ Int
	where
	toQ n = Frac n 1

instance toQ Real
	where
	toQ r
	| toInt (abs r) == 0 = zero
	// Please ignore the following line of code and its magical numbers, there is some logic behind it.
	| otherwise = Frac (entier (r * (toReal (10^(16-(entier (log10 (abs r)) + 1)))))) (10^(16-(entier (log10 (abs r)) + 1)))

instance toQ (Int, Int)
	where
	toQ (t, n)
	| n == 0 = abort "Can't divide by 0"
	| otherwise = Frac t n

instance toQ (Int, Int, Int)
	where
	toQ (x, t, n)
	| n == 0 = abort "Can't divide by 0"
	| otherwise = Frac (x * n + t) n

instance toString Q
	where
	toString (Frac t n) = toString ((t - t rem n) / n) +++ "+" +++ toString (t rem n) +++ "/" +++ toString n

