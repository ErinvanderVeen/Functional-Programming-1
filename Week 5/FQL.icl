module FQL

import StdEnv
import StdT
import Group

::  Nummer          =   { groep :: String         // De naam van de groep
                        , album :: String         // De naam van het album
                        , jaar  :: Int            // Het jaar van uitgave
                        , track :: Int            // Track nummer van het nummer (1 ..)
                        , titel :: String         // Naam van het nummer
                        , tags  :: [String]       // Beschrijvende tags van het nummer / album
                        , lengte:: T              // Lengte van het nummer
                        , land  :: [String]       // Land van oorsprong van groep
                        }

Start world
# (ok,dbs,world)	    = fopen "Nummers.dbs" FReadText world
| not ok	        	= abort "Kon bestand 'Nummers.dbs' niet openen."
# (inhoud,dbs)			= filelines dbs
# (ok,world)			= fclose dbs world
| not ok				= abort "Kon bestand 'Nummers.dbs' niet sluiten na lezen."
# nummersDB				= [  { groep = group
	                         , album = cd
	                         , jaar  = toInt year
	                         , track = toInt track
	                         , titel = title
	                         , tags  = sort (symbol_separated_list ',' tags)
	                         , lengte= fromString length
	                         , land  = sort (symbol_separated_list ',' countries)
	                         }
	                      \\ [_,group,cd,year,track,title,tags,length,countries] <- collect (nr_of_fields+1)		// verzamel alle elementen van een entry
	                                                                                (drop (nr_of_fields+1) 			// verwijder eerste elementen (headers)
	                                                                                (map initString inhoud))		// verwijder alle \n
	                      ]
= (alle_jaarblokken nummersDB,world)
where
	nr_of_fields		= 8

alle_groepen			:: [Nummer] -> [String]
alle_groepen db			= sort (removeDup [g.groep \\ g <- db])

alle_jaarblokken		:: [Nummer] -> [String]
alle_jaarblokken db		= samenvoegen (verwijder (voeg_streepjes_toe (sort (removeDup [n.jaar \\ n <- db]))))
where
    voeg_streepjes_toe                      :: [Int] -> [String]
    voeg_streepjes_toe [i : []]             = [toString i]
    voeg_streepjes_toe [i : ix]
    | i + 1 == ix !! 0                      = [toString i] ++ ["-"] ++ voeg_streepjes_toe ix
    | otherwise                             = [toString i] ++ voeg_streepjes_toe ix

    verwijder                               :: [String] -> [String]
    verwijder []                            = []
    verwijder ["-" : sx]
    | length sx >= 2 && sx !! 1 == "-"      = verwijder ["-" : drop 2 sx]
    | otherwise                             = ["-" : verwijder sx]
    verwijder [s : sx]                      = [s : verwijder sx]

    samenvoegen                             :: [String] -> [String]
    samenvoegen []                          = []
    samenvoegen [s : sx]
    | sx <> [] && sx !! 0 == "-"            = [s +++ "-" +++ sx !! 1 : samenvoegen (drop 2 sx)]
    | otherwise                             = [s : samenvoegen sx]

alle_albums_van			:: String [Nummer] -> [(Int,String)]
alle_albums_van band db	= sortBy (comparator) (removeDup ([(n.jaar, n.album) \\ n <- db | n.groep == band]))
where
    comparator          :: (Int, String) (Int, String) -> Bool
    comparator (a, _) (b, _)      = a < b

alle_tracks				:: String String [Nummer] -> [(Int,String,T)]
alle_tracks cd band db	= sortBy (comparator) [(n.track, n.titel, n.lengte) \\ n <- db | n.groep == band && n.album == cd]
where
    comparator          :: (Int, String, T) (Int, String, T) -> Bool
    comparator (a, _, _) (b, _, _) = a < b

speelduur_albums		:: [Nummer] -> [(T, String, String)]
speelduur_albums     db	= sortBy (comparator) [(speelduur_album t db, snd t, fst t) \\ t <- flatten [alle_albums_van z db \\ z <- alle_groepen db]]
where
    alle_albums_van			:: String [Nummer] -> [(String,String)]
    alle_albums_van band db	= removeDup ([(n.album, band) \\ n <- db | n.groep == band])

    speelduur_album     :: (String, String) [Nummer] -> T
    speelduur_album (s1, s2) db = sum [thd3 z \\ z <- alle_tracks s1 s2 db]

    comparator          :: (T, String, String) (T, String, String) -> Bool
    comparator (a, _, _) (b, _, _) = a < b

totale_speelduur		:: [Nummer] -> String
totale_speelduur     db = (toString (sum [fst3 z \\ z <- speelduur_albums db])) +++ " en dat is: " +++ toString ((toReal (toInt (sum [fst3 z \\ z <- speelduur_albums db])))/86400.0) +++ " dagen."

nederlandse_metal		:: [Nummer] -> [String]
nederlandse_metal    db =  [z \\ z <- removeDup [n.groep \\ n <- db] | isMetalNL z db]
where
    isMetalNL             :: String [Nummer] -> Bool
    isMetalNL z db        = isMember z [n.groep \\ n <- db | n.groep == z && (isMember "metal" n.tags) && (isMember "Netherlands" n.land)]


/*  De volgende functies zijn nodig in de Start functie om het bestand 'Nummers.dbs' in te lezen.
    Je hoeft op dit moment nog niet te begrijpen wat hier staat.
*/

//  filelines leest alle regels in van file.
filelines				:: !*File -> (![String],!*File)
filelines file
# (end,file)			= fend file
| end                   = ([],file)
# (line,file)           = freadline file
# (lines,file)          = filelines file
= ([line:lines],file)

//  initString verwijdert laatste teken aan het einde van een regel.
initString			    :: !String -> String
initString str		    = str%(0,size str-3)

/*  collect n [x_1, ..., x_n, x_{n+1}, ... x_{2n}, ..., x_{mn+1} ... x_{mn+k}]
        = [[x_1, ..., x_n], [x_{n+1}, ... x_{2n}], ..., [x_{(m-1)n+1} ... x_{mn}]]
    waar:
        n > 0 /\ m >= 0 /\ k <= n
*/
collect					:: !Int ![x] -> [[x]]
collect n list
| length groupN < n	    = []
| otherwise			    = [groupN:collect n list`]
where
    (groupN,list`)		= splitAt n list

symbol_separated_list	:: !Char !String -> [String]
symbol_separated_list c str
						= filter (\str -> str <> "" && str <> (toString c)) [toString cs \\ cs <- group ((==) c) (fromString str)]
