implementation module StdT

import StdEnv

::	T = T !Int		// tijd in seconden

instance ==			T where == t1 t2	= toInt t1 == toInt t2
instance <			T where <  t1 t2	= toInt t1 <  toInt t2

instance zero		T where zero		= fromInt 0
instance +			T where +    t1 t2	= fromInt (toInt t1 + toInt t2)
instance -			T where -    t1 t2	= fromInt (max 0 (toInt t1 - toInt t2))

instance toInt		T where toInt (T t)	= t
instance fromInt	T where fromInt t   = T (max 0 t)

instance toString	T where toString (T s) = showtime s
instance fromString	T where fromString s   = T (parsetime s)

showtime			:: !Int -> String
showtime s
| s <= 0			= "0:00"
| otherwise			= toString (s / 60) +++ ":" +++ if (seconds < 10) "0" "" +++ toString seconds
where
	seconds			= s rem 60

/*	Als je nog geen lijst-functies hebt gehad, zul je zelf de "m+:ss" string moeten omzetten.
	Het is handig eerst een algemene functie splitAtChar te maken die een string splitst.
*/
parsetime			:: !String -> Int
parsetime str
| s_str <> ""		= 60 * (toInt m_str) + toInt s_str
| otherwise			= 0
where
	(m_str,s_str)	= splitAtChar ':' str

/** splitAtChar c (a +++ toString c +++ b) = (a,  b)
    splitAtChar c str                      = (str,"")
*/
splitAtChar			:: !Char !String -> (String,String)
splitAtChar c str
| i >= size str		= (str,"")
| otherwise			= (str % (0,i-1), str % (i+1,size str - 1))
where
	i				= seekChar ':' 0 str

/** seekChar c i s zoekt in s vanaf index positie i naar het eerste voorkomen van c.
	Als deze er is, dan wordt de index positie opgeleverd.
	Als deze er niet is, dan wordt de size van s opgeleverd.
*/
seekChar			:: !Char !Int !String -> Int
seekChar c i s
| i >= size s		= size s				// teken niet gevonden, levert size van s op (geen legale index)
| i < 0				= seekChar c 0 s		// begin tenminste vanaf index 0
| s.[i] == c		= i						// gevonden, lever index op
| otherwise			= seekChar c (i+1) s	// niet gevonden, zoek verder naar achteren in s


/*	Als je lijst-functies hebt gehad, dan is dit een kortere oplossing:
parsetime			:: !String -> Int
parsetime str		= case span ((<>) ':') (fromString str) of
						(minutes,[_:seconds])	= 60 * (toInt (toString minutes)) + toInt (toString seconds)
						_						= 0
*/
