implementation module Group

import StdEnv

Start				= group isEven [1..10]
Start				= group isOdd  [1..10]
Start				= group isDigit ['7 Dwergen en 11 prinsessen zoenden 3 kikkerprinsen.']

group				:: (a -> Bool) [a] -> [[a]]
group p []			= []
group p xs			= [yes,no:group p more]
where
	(yes,no_more)	= span p xs
	(no,more)		= span (not o p) no_more
