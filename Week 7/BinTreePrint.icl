implementation module BinTreePrint

import StdEnv
import BinTree

Start = map (flip (+++) "\n" o toString) [t0, t1, t2, t3, t4, t5, t6, t7]

instance toString (Tree a) | toString a where
	toString tree = indentTree tree

indentTree :: !(Tree a) -> String | toString a
indentTree tree = indentTree` tree 0

indentTree` :: !(Tree a) Int -> String | toString a
indentTree` Leaf n = indenting n +++ "Leaf"
indentTree` (Node e left right) n = indenting n +++ "(Node" +++ toString e +++ "\n" +++ indentTree` left (n + 1) +++ "\n" +++ indentTree` right (n + 1) +++ "\n" +++ indenting n +++ ")"

indenting :: Int -> String
indenting 0 = ""
indenting n = "\t" +++ indenting (n - 1)
