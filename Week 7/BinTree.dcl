definition module BinTree

:: Tree a = Node a (Tree a) (Tree a) | Leaf

t0 :: Tree Int
t1 :: Tree Int
t2 :: Tree Int
t3 :: Tree Int
t4 :: Tree Int
t5 :: Tree Int
t6 :: Tree Int
t7 :: Tree Int

nodes  :: (Tree a) -> Int
leaves :: (Tree a) -> Int
diepte :: (Tree a) -> Int

toList :: (Tree a) -> [a]
