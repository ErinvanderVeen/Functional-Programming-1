definition module Perms

import StdClass

perms :: [a] -> [[a]] | Eq a
