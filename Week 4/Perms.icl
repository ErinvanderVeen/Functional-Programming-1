// 1: Het meest algemene type van perms is [a] -> [[a]]
// 2: Er zijn n! permutaties met n elementen.
// 3: Zie hieronder:

implementation module Perms

import StdEnv

Start = perms [1..5]

perms :: [a] -> [[a]] | Eq a
perms [] = [[]]
perms l = [[e] ++ pl \\ e <- l , pl <- perms (removeMember e l)]
