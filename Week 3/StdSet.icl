implementation module StdSet

import StdEnv

:: Set a :== [a]

toSet :: [a] -> (Set a) | Eq a
toSet x = removeDup x

// We need to remember what we have inserted so far to find duplicates so we use a function as a helper.
// This unfortunately gives an error when trying to use memberOfSet. Why?
//removeDuplicates :: (Set a) (Set a) -> (Set a)
//removeDuplicates [] elementsSeen = []
//removeDuplicates [hd:tl] elementsSeen
//| memberOfSet hd elementsSeen = removeDuplicates tl elementsSeen
//| otherwise = [hd] ++ removeDuplicates tl ([hd] ++ elementsSeen)

fromSet :: (Set a) -> [a]
fromSet x = x

isEmptySet :: (Set a) -> Bool
isEmptySet [] = True
isEmptySet _ = False

isDisjoint :: (Set a) (Set a) -> Bool | Eq a
isDisjoint [] x = True
isDisjoint [hd:tl] x
| memberOfSet hd x = False
| otherwise = isDisjoint tl x

isSubset :: (Set a) (Set a) -> Bool | Eq a
isSubset x1 x2 = nrOfElements x2 - nrOfElements x1 == nrOfElements (without x2 x1)

isStrictSubset :: (Set a) (Set a) -> Bool  | Eq a
isStrictSubset x1 x2 = (isSubset x1 x2) && nrOfElements x2 > nrOfElements x1

memberOfSet :: a (Set a) -> Bool | Eq a
memberOfSet x [] = False
memberOfSet x [hd:tl] = hd == x || memberOfSet x tl

union :: (Set a) (Set a) -> Set a | Eq a
union x1 x2 = removeDup (x1 ++ x2)

intersection :: (Set a) (Set a) -> Set a | Eq a
intersection [] x = []
intersection [hd:tl] x
| memberOfSet hd x = [hd] ++ intersection tl x
| otherwise = intersection tl x

nrOfElements :: (Set a) -> Int
nrOfElements [] = 0
nrOfElements [_:tl] = 1 + nrOfElements tl

without :: (Set a) (Set a) -> Set a | Eq a
without x1 x2 = removeMembers x1 x2

product :: (Set a) (Set b) -> Set (a, b)
product x1 x2 = [(a, b) \\ a <- x1, b <- x2]

instance zero (Set a)
	where
	zero = []

instance == (Set a) | Eq a
	where
	(==) (x1) (x2) = (isSubset x1 x2 && isSubset x2 x1)

powerSet :: (Set a) -> Set (Set a)
powerSet x = abort "Not implemented (optional)"

