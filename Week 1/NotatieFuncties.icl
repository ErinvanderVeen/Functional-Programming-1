module NotatieFuncties

import StdEnv

// Always returns the integer 6
f1			:: Int
f1			= 1 + 5

// Always returns the integer 6, but now calculates 1 + 5 with prefix notation
f2			:: Int
f2			= (+) 1 5

// Returns the minimum of the two given arguments
f3			:: Int Int -> Int
f3 m n
| m < n		= m
| otherwise	= n

// Concaternates the given String with itself for a given amount of times
f4			:: String Int -> String
f4 s n
| n <= 0	= ""
| otherwise	= s +++ f4 s (n-1)

// Calculates the Greatest Common Divider
f5			:: Int Int -> Int
f5 x 0		= x
f5 x y		= f5 y (x rem y)

// Returns the sum of the first and second argument of the tuple
f6			:: (Int,Int) -> Int
f6 x		= fst x + snd x

// Returns a tuple where the two elements are switched
f7			:: (a,b) -> (b,a)
f7 (a,b)	= (b,a)

// Returns the original tuple by switching the elements around twice
f8			:: (a,a) -> (a,a)
f8 x		= f7 (f7 x)

Start		= f8 (1,2)
