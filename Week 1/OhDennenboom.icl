implementation module OhDennenboom

import StdEnv

Start = dennenboom 4

dennenboom		:: Int -> String
dennenboom x	= dennenboom` x x

dennenboom`		:: Int Int -> String
dennenboom` 1 y	= "\n" +++ driehoek 1 y
dennenboom` x y	= dennenboom` (x-1) y +++ driehoek x y

driehoek		:: Int Int -> String
driehoek 1 y	= repeatString " " (y-1) +++ "*\n"
driehoek x y	= driehoek (x-1) y +++ repeatString " " (y-x) +++ repeatString "*" (2*x-1) +++ "\n"

// From practicum.zip NotatieFuncties.icl
repeatString	:: String Int -> String
repeatString s n
| n <= 0		= ""
| otherwise		= s +++ repeatString s (n-1)

